import json
import boto3

def endpoint(event, context):
    client = boto3.client('ec2', region_name='eu-west-1')
    msg = "Failed"
    if event['id'] != None:
        response = client.terminate_instances(
            InstanceIds=[
                event['id'],
            ]
        )
        msg = event['id']+" has been deleted"
    return {
        "statusCode": 200,
        "body": msg
    }
